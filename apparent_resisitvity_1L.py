import math
from math import sqrt, log10
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches


def npa_1L_resistivity(Log_rho_1=2, Log_rho_2=2, d=10):
    
    rho_1=10**(Log_rho_1)
    rho_2=10**(Log_rho_2)
    if rho_1 > rho_2:
        color1 = 'dimgrey'
        color2 = 'lightgrey'
    if rho_1 == rho_2:
        color1 = 'darkgrey'
        color2 = 'darkgrey'
    if rho_1 < rho_2:
        color1 = 'lightgrey'
        color2 = 'dimgrey'
        
    K=(rho_2-rho_1)/(rho_2 + rho_1)
    
    #print(rho_1, rho_2, K)
    
    x_max= 100/d
    
    x_all = []
    rho_a_all = []
    x=0
    i=1
    while i < 100:
        x=i/d
        x_all.append(x)
        rho_a = 0
        n=1
        while n < 100:
            rho_a = rho_a + ( 4 * (K**n)/(sqrt( 1 + (2*n*d/i)**2 ) ) ) - ( 2 * (K**n)/(sqrt( 1 + (n*d/i)**2 ) ) )
            n += 1
                                                                        
        rho_a = rho_1 * ( 1 + rho_a )
        rho_a = math.log10(rho_a) 
        rho_a_all.append(rho_a)
        i+=1

    depth=-1.0*d
    fig, (ax1,ax2) = plt.subplots(2,figsize=(15,6))
    ax1.plot(x_all,rho_a_all)
    ax1.grid()
    ax1.set_ylabel('Log10 Apparent Resistivity (Ohm/m)')
    ax1.set_xlim(0,x_max)
    ax1.set_ylim(-0.5,3.5)
    
    
    rect1 = patches.Rectangle((0, -30), x_max, 100, facecolor=color2)
    ax2.add_patch( rect1 )
    rect2 = patches.Rectangle((0, depth), x_max, 100, facecolor=color1)
    ax2.add_patch( rect2 )
    ax2.grid()
    ax2.set_ylabel('Depth (m)')
    ax2.set_xlabel(' Rapporto L/d')
    ax2.set_xlim(0,x_max)
    ax2.set_ylim(-30,0)
    plt.figure()
    
    return

