import math
from math import sqrt, log10
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches


def npa_2L_resistivity(Log_rho=2, Log_rho_M=2):
    
    d1=10
    d2=15
    rho=10**(Log_rho)
    rho_M=10**(Log_rho_M)
    if rho > rho_M:
        color1 = 'dimgrey'
        color2 = 'lightgrey'
    if rho == rho_M:
        color1 = 'darkgrey'
        color2 = 'darkgrey'
    if rho < rho_M:
        color1 = 'lightgrey'
        color2 = 'dimgrey'
       
    # LAYER 1-2
    rho_1=rho
    rho_2=rho_M
    K=(rho_2-rho_1)/(rho_2 + rho_1)
    d=d1
    
    
    x_max= 100/d
    
    # Find where the two curves intersect
    
    intersect=-1
    min_dist=100.
    i=1
    while i < 100:
        x=i/d
        rho_a12 = 0
        rho_a23 = 0
        n=1
        while n < 100:
            # Layer 1-2
            rho_1=rho
            rho_2=rho_M
            K=(rho_2-rho_1)/(rho_2 + rho_1)
            d=d1
            rho_a12 = rho_a12 + ( 4 * (K**n)/(sqrt( 1 + (2*n*d/i)**2 ) ) ) - ( 2 * (K**n)/(sqrt( 1 + (n*d/i)**2 ) ) )
            # Layer 2-3
            rho_1=rho_M
            rho_2=rho
            K=(rho_2-rho_1)/(rho_2 + rho_1)
            d=d2
            rho_a23 = rho_a23 + ( 4 * (K**n)/(sqrt( 1 + (2*n*d/i)**2 ) ) ) - ( 2 * (K**n)/(sqrt( 1 + (n*d/i)**2 ) ) )
            n += 1
                                                                        
        rho_a12 = rho * ( 1 + rho_a12 )
        rho_a23 = rho_M * ( 1 + rho_a23 )
        
        rho_a12 = math.log10(rho_a12)
        rho_a23 = math.log10(rho_a23)
        
        delta_rho=abs(rho_a12-rho_a23)

        if delta_rho < min_dist:
            min_dist=delta_rho
        else:
            intersect=i
            i=101
        i+=1

        
       
    # LAYER 1-2
    rho_1=rho
    rho_2=rho_M
    K=(rho_2-rho_1)/(rho_2 + rho_1)
    d=d1
    
    
    x_max= 100/d

    x_all = []
    rho_a_all_12 = []
    x=0
    i=1
    while i < 100:
        x=i/d
        x_all.append(x)
        rho_a = 0
        n=1
        while n < 100:
            rho_a = rho_a + ( 4 * (K**n)/(sqrt( 1 + (2*n*d/i)**2 ) ) ) - ( 2 * (K**n)/(sqrt( 1 + (n*d/i)**2 ) ) )
            n += 1
                                                                        
        rho_a = rho_1 * ( 1 + rho_a )
        rho_a = math.log10(rho_a) 
        rho_a_all_12.append(rho_a)
        i+=1
        
    # LAYER 2-3
    rho_1=rho_M
    rho_2=rho
    K=(rho_2-rho_1)/(rho_2 + rho_1)
    d=d2
    
    x_max= 100/d
    
    x_all = []
    rho_a_all_23 = []
    x=0
    i=1
    while i < 100:
        x=i/d
        x_all.append(x)
        rho_a = 0
        n=1
        while n < 100:
            rho_a = rho_a + ( 4 * (K**n)/(sqrt( 1 + (2*n*d/i)**2 ) ) ) - ( 2 * (K**n)/(sqrt( 1 + (n*d/i)**2 ) ) )
            n += 1
                                                                        
        rho_a = rho_1 * ( 1 + rho_a )
        rho_a = math.log10(rho_a) 
        rho_a_all_23.append(rho_a)
        i+=1 
    
    rho_a_all_final = []
    i=1
    while i < intersect:
        rho_i=rho_a_all_12[i-1]
        rho_a_all_final.append(rho_i)
        i+=1
    i=intersect
    while i < 100:
        rho_i=rho_a_all_23[i-1]
        rho_a_all_final.append(rho_i)
        i += 1
        
    

    depth1=-1.0*d1
    depth2=-1.0*d2
    fig, (ax1,ax2) = plt.subplots(2,figsize=(15,6))
    ax1.plot(x_all,rho_a_all_12, color='red',label='1-M')
    ax1.plot(x_all,rho_a_all_23, color='blue',label='M-2')
    ax1.plot(x_all,rho_a_all_final, color='green',linewidth=4,label='OBSERVED')
    ax1.grid()
    ax1.legend(loc='upper right')
    ax1.set_ylabel('Log10 Apparent Resistivity (Ohm/m)')
    ax1.set_xlim(0,x_max)
    ax1.set_ylim(-0.5,3.5)
    
    
    rect1 = patches.Rectangle((0, -30), x_max, 100, facecolor=color1)
    ax2.add_patch( rect1 )
    rect2 = patches.Rectangle((0, depth2), x_max, 100, facecolor=color2)
    ax2.add_patch( rect2 )
    rect3 = patches.Rectangle((0, depth1), x_max, 100, facecolor=color1)
    ax2.add_patch( rect3 )
    ax2.grid()
    ax2.set_ylabel('Depth (m)')
    ax2.set_xlabel(' Rapporto L/d')
    ax2.set_xlim(0,x_max)
    ax2.set_ylim(-30,0)
    plt.figure()
    
    return

