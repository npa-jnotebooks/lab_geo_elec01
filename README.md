# LAB_GEO_ELEC01

Jupyter Notebook per studiare la variazione di resistivita' apparente osservata nel caso di uno o due strati orizzontali omogenei¶

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/npa-jnotebooks%2Flab_geo_elec01/HEAD?labpath=UNIMIB_GEO_ELEC01_resistivity.ipynb)
